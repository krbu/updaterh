# CREATED:  2017-10-05 17:25:19
# MODIFIED: 2019-05-27 14:04:00
#
#!/bin/bash - 
#===============================================================================
#
#          FILE: Satellite_attachments.sh
# 
#         USAGE: ./Satellite_attachments.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 10/05/2017 05:25:19 PM
#      REVISION: 
#       * 20/04/2018 - SSB anyadido repo del DRLM para las fisicas
#       * 27/05/2019 - RCP Nou EPEL per satellite 6.4.2 (disabled per defecte)
#===============================================================================
REL=$(lsb_release -r -s | cut -d"." -f1)

#Revisem si la maquina es virtual o fisica
dmidecode -t 1|grep -i vmware
if [[ $? -eq 0 ]]
then
  subscription-manager remove --pool=8aa000c46b414ff6016b41746286065b --pool=8aa000c46b414ff6016b41745ea0060f 
fi

